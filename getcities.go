package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"sync"
	"time"
)

const (
	getRequest string        = "http://www.webservicex.net/globalweather.asmx/GetCitiesByCountry?CountryName="
	delay      time.Duration = 250
	success    string        = "successResponses.txt"
	fail       string        = "badResponses.txt"
)

type Result struct {
	Body      string
	isSuccess bool
}

var (
	mu                      sync.Mutex
	successCount, failCount int
	res                     Result
)

func main() {
	succRes, err := os.OpenFile(success, os.O_APPEND|os.O_CREATE, 0600)
	if err != nil {
		fmt.Printf("Can't create or open file %s: %v\n", success, err)
		return
	}
	badRes, err := os.OpenFile(fail, os.O_APPEND|os.O_CREATE, 0600)
	if err != nil {
		fmt.Printf("Can't create or open file %s: %v\n", fail, err)
		return
	}
	defer succRes.Close()
	defer badRes.Close()
	ch := make(chan Result)
	countries := os.Args[1:]
	fmt.Println(countries)
	for _, country := range countries {
		req := getRequest + country
		go fetch(req, ch)
		time.Sleep(delay * time.Millisecond)
	}
	for res := range ch {
		if res.isSuccess {
			succRes.WriteString(res.Body)
		} else {
			badRes.WriteString(res.Body)
		}
	}
	failCount = len(countries) - successCount
	fmt.Println("Success requests count: ", successCount)
	fmt.Println("Fail requests count: ", failCount)
}

func fetch(req string, ch chan Result) {
	res := Result{}
	resp, err := http.Get(req)
	if err != nil {
		res.Body = fmt.Sprintf("Can't get %s request: %v\n", req, err)
		ch <- res
		return
	}
	b, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		res.Body = fmt.Sprintf("Body for request %s: %v\n", req, err)
		ch <- res
		return
	}
	res.Body = fmt.Sprintf("%s", b)
	res.isSuccess = true
	mu.Lock()
	successCount++
	mu.Unlock()
	ch <- res
}
